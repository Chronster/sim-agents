﻿using UnityEngine;
using System.Collections;

public class Globals : MonoBehaviour {

	public static readonly int zero = 0;
	public static int inf = (1/zero);	// infinity
}
