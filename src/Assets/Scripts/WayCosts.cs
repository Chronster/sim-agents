﻿using UnityEngine;
using System.Collections;

public class WayCosts : MonoBehaviour {
	public static readonly float[,] costs = new float[,] {
		{0.0f, 2.0f, 1.0f, 2.0f},
		{2.0f, 0.0f, 1.0f, 2.0f},
		{1.0f, 1.0f, 0.0f, 1.0f},
		{2.0f, 2.0f, 1.0f, 0.0f}
	};
}
