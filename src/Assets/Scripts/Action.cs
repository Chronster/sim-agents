﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Action {
	public int type;
	public string label;
	public List<int> preconditions = new List<int>();
	public float[] effects = new float[Motivations.motivationCount];
	public float duration;

	// Use this for initialization
	void Start () {
		effects = new float[Motivations.motivationCount];
	}
}
