﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GeneratePlanningTree {
	static Node start = new Node();
	//static GameObject sim;
	static List<float> steps;
	static List<float> heuristicCoefficients;
	static List<Node> planningQueue = new List<Node> ();
	static int horizon;
	static float bestRating;
	//private static readonly int zero = 0;
	//private static int inf = (1/zero);	// infinity

	// Use this for initialization
	public static void computeMoves (int room, GameObject character) {
		//sim = character;
		bestRating = -99999999.99f;
		start = new Node ();
		start.room = Rooms.livingroom;
		start.depth = 0;
		start.rating = 0.0f;
		start.stats = new List<float> (character.GetComponent<Motivations> ().stats);
		//start.inventory = new int[Rooms.inventory.GetLength(0),Rooms.inventory.GetLength(1)](Rooms.inventory);
		start.inventory = (int[,])Rooms.inventory.Clone();
		steps = new List<float> (character.GetComponent<Motivations> ().steps);
		heuristicCoefficients = new List<float> (character.GetComponent<Motivations> ().heuristicCoefficients);
		horizon = character.GetComponent<Motivations> ().searchDepth;
		//Debug.Log ("Horizon: " + horizon);
		generateTree (start);
		planningQueue.Reverse ();

		for (int i=0; i<planningQueue.Count; i++) {
			Debug.Log (planningQueue [i].action.label + ", " + planningQueue[i].rating);
			for (int j=0; j<planningQueue[i].action.preconditions.Count; j++) {
				if (Rooms.inventory[planningQueue[i].room, planningQueue[i].action.preconditions[j]] > 0) {
					--Rooms.inventory[planningQueue[i].room, planningQueue[i].action.preconditions[j]];
				}
			}
		}

		Debug.Log (planningQueue.Count);
		Node childNode = planningQueue [planningQueue.Count-1];
		Debug.Log (childNode.action.label + ", " + childNode.rating + ", " + childNode.depth + "thirst: " + childNode.stats[Motivations.thirst] + "fullness: " + childNode.stats[Motivations.fullness] + "fun: " + childNode.stats[Motivations.fun] + "tiredness: " + childNode.stats[Motivations.tiredness] + "hygiene: " + childNode.stats[Motivations.hygiene]);

		character.GetComponent<Motivations> ().plan = new List<Node>(planningQueue);
	}

	static void generateTree(Node currentNode) {
		List<int> roomList = new List<int> ();
		//float rating;

		//float heuristic;
		//float wayCosts;

		if (currentNode.depth == horizon - 1) {
			if (currentNode.rating > bestRating) {
				Node parent = currentNode;
				planningQueue.Clear();
				planningQueue.Add(currentNode);
				//planningQueue.Add (currentNode.parent);
				//planningQueue.Add (currentNode.parent.parent);
				bestRating = currentNode.rating;
				for (int i=0; i<horizon-2; i++) {
					parent = parent.parent;
					planningQueue.Add(parent);
				}
		    }
			return;
		}

		for (int i=0; i<Actions.actions.Count; i++) {
			roomList.Clear();
			roomList = findRoomsWithPreconditions(currentNode, Actions.actions[i].preconditions);
			for (int j=0; j<roomList.Count; j++) {
				Node childNode = new Node();
				childNode.action = Actions.actions[i];
				childNode.parent = currentNode;
				childNode.room = roomList[j];
				childNode.depth = currentNode.depth+1;
				childNode.stats = new List<float> (currentNode.stats);
				childNode.rating = childNode.parent.rating;
				childNode.inventory = (int[,])currentNode.inventory.Clone();
				for (int k=0; k<childNode.action.preconditions.Count; k++) {
					if (childNode.inventory[childNode.room, childNode.action.preconditions[k]] > 0) {
						childNode.inventory[childNode.room, childNode.action.preconditions[k]]--;
					}
				}
				for (int k=0; k<steps.Count; k++) {
					// calculate future stats after moving to this room
					childNode.stats[k] += steps[k] * WayCosts.costs[currentNode.room, childNode.room];
					childNode.stats[k] = Mathf.Clamp01(childNode.stats[k]);
					// calculate future stats after executing the action (incl. loss through duration)
					childNode.stats[k] += childNode.action.effects[k] + steps[k] * childNode.action.duration;
					childNode.stats[k] = Mathf.Clamp01(childNode.stats[k]);
					childNode.rating += childNode.stats[k] * heuristicCoefficients[k];
				}
				//Debug.Log (childNode.action.label + ", " + childNode.rating + ", " + childNode.depth + "thirst: " + childNode.stats[Motivations.thirst] + "fullness: " + childNode.stats[Motivations.fullness] + "fun: " + childNode.stats[Motivations.fun] + "tiredness: " + childNode.stats[Motivations.tiredness] + "hygiene: " + childNode.stats[Motivations.hygiene]);
				//childNode.rating = rating;
				currentNode.children.Add (childNode);
				generateTree (childNode);
			}
		}
	}

	static List<int> findRoomsWithPreconditions(Node currentNode, List<int> preconditions) {
		List<int> rooms = new List<int>();
		int currentInventory = -1;
		int foundItemsCount = 0;
		//Debug.Log (Rooms.roomCount);

		// If there is no precondition, action can be taken in all rooms
		if (preconditions.Count == 0) {
			for (int i=0; i<Rooms.roomCount; i++) {
				rooms.Add(i);
			}
		}

		// Else, search for all rooms that match the precondition(s)
		for (int i=0; i<Rooms.roomCount; i++) {
			foundItemsCount = 0;
			for (int j=0; j<preconditions.Count; j++) {
				//Debug.Log (preconditions[j]);
				currentInventory = currentNode.inventory[i, preconditions[j]];
				if (currentInventory != 0) {
					//Debug.Log ("found precondition in room " + i); 
					foundItemsCount++; 
				}
				if (foundItemsCount == preconditions.Count) {
					rooms.Add(i);
				}
			}
		}

		return rooms;

	}
}