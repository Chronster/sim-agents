﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Motivations : MonoBehaviour {

	[Range(0.0f, 1.0f)]
	public float thirstValue = 0.5f;
	public float thirstStep = 0.05f;
	public float thirstCoefficient = -30000.00f;
	public const int thirst = 0;
	[Range(0.0f, 1.0f)]
	public float fullnessValue = 0.4f;
	public float fullnessStep = -0.05f;
	public float fullnessCoefficient = 35000.00f;
	public const int fullness = 1;
	[Range(0.0f, 1.0f)]
	public float funValue = 0.5f;
	public float funStep = -0.05f;
	public float funCoefficient = 40000.00f;
	public const int fun = 2;
	[Range(0.0f, 1.0f)]
	public float tirednessValue = 0.0f;
	public float tirednessStep = 0.05f;
	public float tirednessCoefficient = -20000.00f;
	public const int tiredness = 3;
	[Range(0.0f, 1.0f)]
	public float hygieneValue = 0.3f;
	public float hygieneStep = -0.05f;
	public float hygieneCoefficient = 10000.00f;
	public const int hygiene = 4;

	public const int motivationCount = 5;
	public int room = Rooms.livingroom;
	public List<float> stats = new List<float>();
	public List<float> steps = new List<float>();
	public List<float> heuristicCoefficients = new List<float>();
	public List<Node> plan = new List<Node>();
	public int searchDepth = 2;
	public int guiOffsetX = 0;
	public int guiOffsetY = 0;
	public string simName = "Default Sim";

	private float actionTime = 0.0f;
	private float wayTime = 0.0f;
	private string simAction;

	private List<string> motivationLabels = new List<string>();
	private GUIStyle boldStyle = new GUIStyle();

	// Use this for initialization
	void Start () {
		stats.Add (thirstValue);
		stats.Add (fullnessValue);
		stats.Add (funValue);
		stats.Add (tirednessValue);
		stats.Add (hygieneValue);

		steps.Add (thirstStep);
		steps.Add (fullnessStep);
		steps.Add (funStep);
		steps.Add (tirednessStep);
		steps.Add (hygieneStep);

		heuristicCoefficients.Add (thirstCoefficient);
		heuristicCoefficients.Add (fullnessCoefficient);
		heuristicCoefficients.Add (funCoefficient);
		heuristicCoefficients.Add (tirednessCoefficient);
		heuristicCoefficients.Add (hygieneCoefficient);

		motivationLabels.Add ("Thirst:");
		motivationLabels.Add ("Fullness:");
		motivationLabels.Add ("Fun:");
		motivationLabels.Add ("Tiredness:");
		motivationLabels.Add ("Hygiene:");

		boldStyle.fontStyle = FontStyle.Bold;
		boldStyle.normal.textColor = Color.white;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float deltaTime = Time.deltaTime;
		if (plan.Count == 0) {
			GeneratePlanningTree.computeMoves (room, this.gameObject);
		} 

		/*thirstValue += thirstStep * deltaTime;
		fullnessValue += fullnessStep * deltaTime;
		funValue += funStep * deltaTime;
		tirednessValue += tirednessStep * deltaTime;
		hygieneValue += hygieneStep * deltaTime;

		thirstValue = Mathf.Clamp01 (thirstValue);
		fullnessValue = Mathf.Clamp01 (fullnessValue);
		funValue = Mathf.Clamp01 (funValue);
		tirednessValue = Mathf.Clamp01 (tirednessValue);
		hygieneValue = Mathf.Clamp01 (hygieneValue);*/
		for (int i=0; i<stats.Count; i++) {
			stats[i] += steps[i] * deltaTime;
			stats[i] = Mathf.Clamp01 (stats[i]);
		}


		//actionTime += deltaTime;
		if (room == plan [0].room) {
			actionTime += deltaTime;
			simAction = "Is " + plan[0].action.label + " in " + Rooms.getRoomLabel(room);
			for (int i=0; i<stats.Count; i++) {
				stats[i] += (plan[0].action.effects[i]/plan[0].action.duration) * deltaTime;
				stats[i] = Mathf.Clamp01 (stats[i]);
			}
		} else {
			wayTime += deltaTime;
			simAction = "Is walking to " + Rooms.getRoomLabel(plan[0].room);
			if (wayTime >= WayCosts.costs[room, plan [0].room]) {
				room = plan [0].room;
				wayTime = 0.0f;
			}
		}

		if (actionTime >= plan[0].action.duration) {
			plan.RemoveAt(0);
			actionTime = 0.0f;
		}
	}

	void OnGUI () {
		GUI.Label (new Rect (guiOffsetX + 25, guiOffsetY + 5, 100, 30), simName + ":", boldStyle);
		GUI.Label (new Rect (guiOffsetX + 25, guiOffsetY + 25, 200, 30), simAction);
		for (int i=0; i<stats.Count; i++) {
			GUI.Label (new Rect (guiOffsetX + 25, guiOffsetY + 25*(i+2), 100, 30), motivationLabels[i] );
			GUI.HorizontalSlider (new Rect (guiOffsetX + 100, guiOffsetY + 25*(i+2)+6, 100, 30), stats[i], 0.0f, 1.0f);
			GUI.Label (new Rect (guiOffsetX + 250, guiOffsetY + 25*(i+2), 50, 30), (stats[i]*100).ToString( "0.00") );
		}
	}
}
