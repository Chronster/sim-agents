﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Node {
	public Action action;
	public Node parent = null;
	public int room;
	public float rating = 0;
	public List<float> stats = new List<float>();
	public List<Node> children = new List<Node>();
	public int depth;
	public int[,] inventory;
}
