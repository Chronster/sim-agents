﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Actions {
	public static List<Action> actions = new List<Action>();
	public const int drinkWater 	= 	0;
	public const int drinkBeer 		= 	1;
	public const int eatSoup 		= 	2;
	public const int eatPizza 		= 	3;
	public const int watchTv 		= 	4;
	public const int playComputer 	= 	5;
	public const int sleepBed 		= 	6;
	public const int sleepCouch 	= 	7;
	public const int brushTeeth 	=	8;
	public const int shower 		= 	9;


	// Use this for initialization
	public static void Init () {

		Action _drinkWater = new Action ();
		_drinkWater.type = drinkWater;
		_drinkWater.label = "drinking water";
		_drinkWater.preconditions.Add (Consumables.water);
		//_drinkWater.effects = new float[Motivations.motivationCount];
		_drinkWater.effects [Motivations.thirst] = -0.5f;
		_drinkWater.duration = 2.0f;
		actions.Add (_drinkWater);

		Action _drinkBeer= new Action ();
		_drinkBeer.type = drinkBeer;
		_drinkBeer.label = "drinking beer";
		_drinkBeer.preconditions.Add (Consumables.beer);
		_drinkBeer.effects [Motivations.thirst] = -0.4f;
		_drinkBeer.effects [Motivations.fun] = +0.5f;
		_drinkBeer.effects [Motivations.tiredness] = +0.2f;
		_drinkBeer.duration = 4.0f;
		actions.Add (_drinkBeer);

		Action _eatSoup = new Action ();
		_eatSoup.type = eatSoup;
		_eatSoup.label = "eating soup";
		_eatSoup.preconditions.Add (Consumables.soup);
		_eatSoup.effects [Motivations.thirst] = -0.25f;
		_eatSoup.effects [Motivations.fullness] = +0.25f;
		_eatSoup.duration = 5.0f;
		actions.Add (_eatSoup);

		Action _eatPizza = new Action ();
		_eatPizza.type = eatPizza;
		_eatPizza.label = "eating pizza";
		_eatPizza.preconditions.Add (Consumables.pizza);
		_eatPizza.effects [Motivations.fullness] = +0.7f;
		_eatPizza.duration = 5.0f;
		actions.Add (_eatPizza);

		Action _watchTv = new Action ();
		_watchTv.type = watchTv;
		_watchTv.label = "watching tv";
		_watchTv.preconditions.Add (Consumables.tv);
		_watchTv.effects [Motivations.fun] = +0.4f;
		_watchTv.duration = 7.0f;
		actions.Add (_watchTv);

		Action _playComputer = new Action ();
		_playComputer.type = playComputer;
		_playComputer.label = "playing computer";
		_playComputer.preconditions.Add (Consumables.computer);
		_playComputer.effects [Motivations.fun] = +0.75f;
		_playComputer.duration = 10.0f;
		actions.Add (_playComputer);

		Action _sleepBed = new Action ();
		_sleepBed.type = sleepBed;
		_sleepBed.label = "sleeping in bed";
		_sleepBed.preconditions.Add (Consumables.bed);
		_sleepBed.effects [Motivations.tiredness] = -1.0f;
		_sleepBed.duration = 20.0f;
		actions.Add (_sleepBed);

		Action _sleepCouch = new Action ();
		_sleepCouch.type = sleepCouch;
		_sleepCouch.label = "sleeping on couch";
		_sleepCouch.preconditions.Add (Consumables.couch);
		_sleepCouch.effects [Motivations.tiredness] = -0.5f;
		_sleepCouch.duration = 10.0f;
		actions.Add (_sleepCouch);

		Action _brushTeeth = new Action ();
		_brushTeeth.type = brushTeeth;
		_brushTeeth.label = "brushing teeth";
		_brushTeeth.preconditions.Add (Consumables.toothbrush);
		_brushTeeth.effects [Motivations.hygiene] = +0.3f;
		_brushTeeth.duration = 3.0f;
		actions.Add (_brushTeeth);

		Action _shower = new Action ();
		_shower.type = shower;
		_shower.label = "taking a shower";
		_shower.preconditions.Add (Consumables.shower);
		_shower.effects [Motivations.hygiene] = +0.7f;
		_shower.duration = 5.0f;
		actions.Add (_shower);
	}
}
