﻿using UnityEngine;
using System.Collections;

public class Consumables : MonoBehaviour {
	public const int water 		= 	0;	
	public const int beer		= 	1;
	public const int soup 		= 	2;
	public const int pizza 		= 	3;
	public const int tv 		= 	4;
	public const int computer 	= 	5;
	public const int bed 		= 	6;
	public const int couch 		= 	7;
	public const int toothbrush = 	8;
	public const int shower 	= 	9;
}
