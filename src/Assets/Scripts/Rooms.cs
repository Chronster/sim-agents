﻿using UnityEngine;
using System.Collections;

public class Rooms : MonoBehaviour {
	//private static readonly int zero = 0;
	//private static int inf = (1/zero);	// infinity
	private const int inf = -1;
	public const int kitchen = 0;	
	public const int bathroom = 1;
	public const int livingroom = 2;
	public const int bedroom = 3;

	public static readonly int[,] inventory = new int[,] {
		//	water	beer	soup	pizza	tv		pc		bed		couch	tooth-	shower
		// 																	brush	
			{inf, 	3, 		inf, 	1, 		0, 		0,		0,		0,		0, 		0,},		// kitchen
			{inf, 	0, 		0, 		0, 		0, 		0,		0,		0,		inf, 	inf,},		// bathroom
			{0, 	0, 		0, 		0, 		inf, 	inf,	0,		inf,	0, 		0,},		// livingroom
			{0, 	0, 		0, 		0, 		0, 		0,		inf,	0,		0, 		0,}			// bedroom
	};

	public static int roomCount = inventory.GetLength (0);

	public static string getRoomLabel(int roomNumber) {
		switch (roomNumber) {
		case 0:
			return "kitchen";
		case 1:
			return "bathroom";
		case 2:
			return "livingroom";
		case 3:
			return "bedroom";
		default:
			return "";
		}
	}
}
