﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {
	public GameObject sim;

	// Use this for initialization
	void Start () {
		//Debug.Log ("start");
		Actions.Init ();
		GeneratePlanningTree.computeMoves (Rooms.livingroom, sim);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Escape))
		{
			Application.Quit();
		}
	}
}
